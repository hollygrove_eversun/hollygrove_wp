<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/home/hollygrovepoint2/public_html/wp-content/plugins/wp-super-cache_/' ); //Added by WP-Cache Manager
define('DB_NAME', 'hollygro_nscyg');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-0Sd4ES(jxtITmFzs69J;Di&thlL#<39QRRK`AU72m0h1X}7]srU_q|JyiaQo9*b');
define('SECURE_AUTH_KEY',  '+pu4|`C|r3EwtN#]MGI!mUao{-DL8| @_6xU,-*+<fsRvo#|N}((?}UA3D=Ib??f');
define('LOGGED_IN_KEY',    'uyqaW4p)YOL[|G>h:PszKhA(1gB|ZFR|[l=)f !O6Vc<yC|zltqEfZoTC+NzH/J|');
define('NONCE_KEY',        '[+~zMu+*+*LgN&&)e!^.=kv+wmnZ)H&4Uh vTjSCQ8/q^){HG1}ly6+eBGq=>_CM');
define('AUTH_SALT',        ',lq[gY@ZR8H!^KB~L&pVz|(#TMY4IHWt9@:hW{j{o{s3+V<}PXR+zzdvfT)|OQ}[');
define('SECURE_AUTH_SALT', 'UPQ5@{[jRI?RAbp-7yxb/o*msFdCtB=_4 q@1wvvu7DKucd|cb2gHL$<E=)Q[ay+');
define('LOGGED_IN_SALT',   'u3*$R4bG]+WXP{J|~l^|LG-+=q,Wg`Hq?, @t8+uzF=5 ]VJSP,eV9osa9.3f8?B');
define('NONCE_SALT',       'q2,IR/cr}OW&E[1mT&%T$R4|%/?=?*PA+p$4B dSe|[liISQvHj4)|J)&W$NWavt');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

define('WP_MEMORY_LIMIT', '512M');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
